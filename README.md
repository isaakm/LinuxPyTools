LinuxPyTools is a small Python library with useful functions for Linux environments.
It currently also contains functions for the Gnome desktop environment.

It's currently in beta


It's licensed under the open source EUPL v1.2 license

See https://joinup.ec.europa.eu/collection/eupl/eupl-text-11-12