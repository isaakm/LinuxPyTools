"""
Copyright 2018 Leonardo Malik
This software is provided to you under the EUROPEAN UNION PUBLIC LICENCE v1.2
See https://joinup.ec.europa.eu/collection/eupl/eupl-text-11-12 for official translations in other languages
"""
import LinuxFunctions
import re
import subprocess


def get_gnome_version():
    """Returns the Gnome Shell version as a MatchObject instance or False when not Gnome Shell"""
    try:
        gnome_version = LinuxFunctions.execute("gnome-shell --version")

        return re.match(r"GNOME Shell (\d).(\d+)(.(\d+))?", gnome_version, re.I)
    except subprocess.CalledProcessError:
        return False


def get_gnome_setting(path, setting):
    """gsetting setting retrieval function. Return digits as ints, strings as string, ..."""
    result = LinuxFunctions.execute("gsettings get {} {}".format(path, setting))

    if re.match("uint32", result) or re.match("\d+", result):
        return int(re.search("^(uint32 )?(\d+)", result)[2])
    elif re.match("'[a-z-]+'", result, re.I):
        return str(re.search("'([a-z-]+)'", result, re.I)[1])
    elif result == "true":
        return True
    elif result == "false":
        return False
    else:
        return result


def set_gnome_setting(path, setting, value):
    """Set a Gnome setting and return true or false on success or failure, by checking with get_gnome_setting()"""
    value = "true" if value is True else value
    value = "false" if value is False else value
    LinuxFunctions.execute("gsettings set {} {} {}".format(path, setting, value))

    if get_gnome_setting(path, setting) == value:
        return True
    else:
        return False


"""
---------------------------------------------
Ready-to-use configuration functions
---------------------------------------------
"""


def get_monitor_timeout():
    """Get only the seconds from the timeout setting"""
    return get_gnome_setting("org.gnome.desktop.session", "idle-delay")


def set_monitor_timeout(minutes):
    seconds = minutes * 60
    set_gnome_setting("org.gnome.desktop.session", "idle-delay", seconds)
