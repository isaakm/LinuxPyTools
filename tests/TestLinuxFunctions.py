"""
Copyright 2018 Isaak Malik
This software is provided to you under the EUROPEAN UNION PUBLIC LICENCE v1.2
See https://joinup.ec.europa.eu/collection/eupl/eupl-text-11-12 for official translations in other languages
"""
from unittest import TestCase
from LinuxFunctions import *
from os import uname
from shutil import which


class TestLinuxFunctions(TestCase):
    def test_app_exists_false(self):
        self.assertFalse(app_exists("blabliblo"))
        self.assertFalse(app_exists("chmod5"))
        self.assertFalse(app_exists("grepz"))

    def test_app_exists_true(self):
        """Some tests of commands that exist in every Linux distro"""
        self.assertTrue(app_exists("which"))
        self.assertTrue(app_exists("chmod"))
        self.assertTrue(app_exists("grep"))
        self.assertTrue(app_exists("cat"))

    def test_package_exists_false(self):
        self.assertFalse(package_exists("thisisonebigfake123"))
        self.assertFalse(package_exists("couldnotbemorefalsethanthis"))

    def test_package_exists_true(self):
        self.assertTrue(package_exists("nano"))
        self.assertTrue(package_exists("grep"))

    def test_execute(self):
        self.assertEqual(uname()[1], execute("hostname"))
        self.assertEqual(which("chmod"), execute("which {}".format("chmod")))
