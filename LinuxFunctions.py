"""
Copyright 2018 Leonardo Malik
This software is provided to you under the EUROPEAN UNION PUBLIC LICENCE v1.2
See https://joinup.ec.europa.eu/collection/eupl/eupl-text-11-12 for official translations in other languages
"""
import os
import re
import shutil
import subprocess

# Needed for execute() to run the command as root. It's clearer than passing True
AS_ROOT = True

# TODO: Add support for languages other than English


def execute(command, as_root=False):
    """Execute a command as regular or root user"""
    command = ("sudo " if as_root else "") + command
    result = subprocess.check_output("{}".format(command), shell=True)

    return result.decode().strip()


def is_root():
    """This is to check whether your script was executed with root privileges"""
    return True if os.getegid() == 0 else False


def distro_is_arch():
    if _check_osrelease_name() == "arch":
        return True

    return False


def distro_is_antergos():
    if _check_osrelease_name() == "antergos":
        return True

    return False


def distro_is_debian():
    if _check_osrelease_name() == "debian":
        return True


def distro_is_redhat():
    pass


def distro_is_fedora():
    pass


def distro_is_centos():
    pass


def distro_is_ubuntu():
    if _check_osrelease_name() == "ubuntu":
        return True

    return False


def distro_is_opensuse():
    if _check_osrelease_name() == "opensuse":
        return True

    return False


def distro_is_debian_based():
    if _check_osrelease_idlike() == "debian":
        return True

    return False


def distro_is_redhat_based():
    if distro_is_fedora() or distro_is_centos() or distro_is_redhat():
        return True

    return False


def distro_is_arch_based():
    if _check_osrelease_idlike() == "arch":
        return True

    return False


def is_gnome():
    try:
        execute("gnome-shell --version &> /dev/null")
        return True
    except subprocess.CalledProcessError:
        return False


def is_kde():
    try:
        execute("plasmashell --version &> /dev/null")
        return True
    except subprocess.CalledProcessError:
        return False


def is_virtual_machine():
    """Is the script run in a virtual machine instead of a bare metal OS?"""
    try:
        result = execute("grep -q ^flags.*\ hypervisor /proc/cpuinfo && echo True")
        return True if result == "True" else False
    except subprocess.CalledProcessError:
        return False


def app_exists(name):
    """Is this application installed? Not compatible with non-application packages, e.g. icon themes"""
    try:
        # &> /dev/null suppresses the output message because we don't need that many details
        execute("which {} &> /dev/null".format(name))
        return True
    except subprocess.CalledProcessError:
        return False


def package_exists(name):
    """
    This is different from app_exists() in the way that it can also
    check for packages that are not applications

    Currently only works on Arch and debian based distros

    TODO: Fedora, SUSE support
    """
    try:
        # &> /dev/null suppresses the output message because we don't need that many details
        if distro_is_arch_based():
            execute("pacman -Qi {} &> /dev/null".format(name))
            return True
        elif distro_is_debian_based():
            execute("dpkg -s {} &> /dev/null".format(name))
            return True
        else:
            raise NotImplementedError("This distro is not supported yet")
    except subprocess.CalledProcessError:
        return False


def install_package(name, thirdparty=False):
    """
    Install a package from the main repos or third party ones like AUR if enabled

    Currently only works on Arch or Debian based distros

    TODO: Fedora, SUSE support
    """
    try:
        if distro_is_arch_based():
            execute("pacman -S --noconfirm --quiet {} &> /dev/null".format(name), AS_ROOT)
        elif distro_is_debian_based():
            execute("apt install -y {}".format(name), AS_ROOT)
        else:
            raise NotImplementedError("This distro is not supported yet")

        return True
    except subprocess.CalledProcessError:
        # Try third party ones, currently only AUR
        if thirdparty and distro_is_arch_based():
            # Prioritise the better ones
            if app_exists("pikaur"):
                aur_helper = "sudo pikaur"
                options = "--noedit"
            elif app_exists("pacaur"):
                aur_helper = "pacaur"
                options = "--silent --noedit"
            elif app_exists("yaourt"):
                aur_helper = "yaourt"
                options = ""

            else:
                return False

            try:
                # Here we're exceptionally passing sudo manually, because pacaur doesn't
                # work the usual way with sudo pacaur
                execute("{} -S --noconfirm {} {}".format(aur_helper, options, name))
                return True
            except subprocess.CalledProcessError:
                return False
        # Installation failed
        return True


def remove_package(name, skip_dependencies=False):
    """Remove a package. AUR is not supported at this time"""
    try:
        if distro_is_arch():
            # Remove package and exclusive dependencies if skip_dependencies is not set
            deps = "" if skip_dependencies else "s"
            execute("pacman -R{} --noconfirm {} &> /dev/null".format(deps, name), AS_ROOT)
        elif distro_is_debian_based():
            execute("apt remove -y {}".format(name), AS_ROOT)
        else:
            raise NotImplementedError("This distro is not supported yet")

        return True
    # Removal failed
    except subprocess.CalledProcessError:
        return False


# TODO: support for more distros
# def are_updates_available():
#     """
#     Check if there are updates available. Currently supported on AMD64 and ARM machines
#
#     @:returns  True or False or -1 if the distro is not supported
#     """
#     if distro_is_debian_based():
#         output = execute("apt update && apt list --upgradeable &> /dev/null", AS_ROOT)
#
#         # A simple way to check for a listed update. Is this failproof?
#         if re.match(r"amd64|amrhf \[", output):
#             return True
#         else:
#             return False
#     elif distro_is_arch_based():
#         output = execute("pacman -Syu", AS_ROOT)
#
#         print(output)
#     else:
#         return -1


def check_dependency(app):
    """Check if one or all application dependenc(y|ies) for a function are met"""
    if isinstance(app, str):
        if shutil.which(app) is None:
            raise Exception("Dependency '{}' is not met for this function".format(app))
    elif isinstance(app, list):
        for c in app:
            check_dependency(c)
    else:
        raise ValueError("arg1 '{}' is neither a string or list".format(app))


def enable_service(service, as_regular_user=False):
    try:
        if as_regular_user:
            execute("systemctl --user enable {}.service &> /dev/null".format(service))
        else:
            execute("systemctl enable {}.service &> /dev/null".format(service), AS_ROOT)

        return True
    except subprocess.CalledProcessError:
        return False


def is_service_enabled(service):
    try:
        execute("systemctl is-enabled {}.service".format(service))
        return True
    except subprocess.CalledProcessError:
        return False


def start_service(service, as_regular_user=False):
    try:
        if as_regular_user:
            execute("systemctl --user start {}.service &> /dev/null".format(service))
        else:
            execute("systemctl start {}.service &> /dev/null".format(service), AS_ROOT)

        return True
    except subprocess.CalledProcessError:
        return False


def is_service_started(service):
    try:
        execute("systemctl is-active --quiet {}.service".format(service))
        return True
    except subprocess.CalledProcessError:
        return False
    

def text_to_green(string):
    """Make (part of) text appear in green in the terminal"""
    return "\033[0;32m{}\033[0m".format(string)


def print_green(string):
    """Make all text appear in green in the terminal"""
    print(text_to_green(string))


def text_to_red(string):
    """Make (part of) text appear in red in the terminal"""
    return "\033[0;31m{}\033[0m".format(string)


def print_red(string):
    """Make all text appear in red in the terminal"""
    print(text_to_red(string))


def text_in_bold(string):
    """Make (part of) text appear in bold in the terminal"""
    return "\033[1m{}\033[0m".format(string)


def which_drives_are_hdds():
    """
    Look for hard disk drives and create a dict of them with their serial number as value.
    Use the serial number and model to find the config file in /etc/udisks2 if it exists and add
    it to the dict.

    Only tested with SATA drives!
    """
    check_dependency("lsblk")

    result = execute("lsblk -dno name,rota,serial")
    result = result.split("\n")
    hddlist = {}

    for line in result:
        if re.match(r"sd[a-z]\s+1", line):
            # Remember device and serial and build the location of the file for later
            device = "/dev/" + line[0:3]
            serial = re.search(r"(\w+)$", line)[1]
            # filename = "/etc/udisks2/" + _convert_to_filename(_find_drive_model(device), serial)
            hddlist[device] = serial

    return hddlist


def read_file_as_root(file):
    return execute("cat {}".format(file), AS_ROOT)


def write_file_as_root(file, contents, append=False):
    check_dependency("tee")
    return execute("echo \"{}\" | sudo tee {} {}".format(contents, ("-a" if append else ""), file), AS_ROOT)


def show_notification(msg, timeout=5000):
    """Show a notification on the desktop"""
    check_dependency("notify-send")
    return execute("notify-send \"{}\" -t {}".format(msg, timeout))


"""
=====================
 Private functions
=====================
"""


def _convert_to_filename(model, serial):
    """Convert a HDD model to a /etc/udisks2 compatible filename"""
    result = model.replace(" ", "-") + "-" + serial + ".conf"
    return result


def _find_drive_model(drive):
    """Get the drive model by using hdparm"""
    check_dependency("hdparm")

    if not re.match("^/dev/s|hd[a-z]$", drive):
        raise Exception("arg1 didn't get a proper device: {}".format(drive))

    model = execute("hdparm -I {} | grep \"Model Number\"".format(drive), AS_ROOT)
    model = re.search(r"\s+(\w+\s?\w*)\s+$", model)
    return model[1]


def _check_osrelease_name():
    if os.path.exists("/etc/os-release"):
        with open("/etc/os-release", "r") as file:
            return re.search(r"id=\"(\w+)", file.read(), re.I)[1].lower()


def _check_osrelease_idlike():
    if os.path.exists("/etc/os-release"):
        with open("/etc/os-release", "r") as file:
            return re.search(r"id_like=\"(\w+)", file.read(), re.I)[1].lower()
